from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect
from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.http import JsonResponse


class ViewUserLogin(TemplateView):
    template_name = 'login.html'

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated():
            return HttpResponseRedirect('/main')
        return super().dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        values = request.POST
        print (values)
        user = authenticate(username=values['user'], password=values['pass'])
        if user is None:
            data = {'login': False, 'msg': 'Usuario o contraseña inválidos'}
            return JsonResponse(data, safe=False)

        if not user.is_active:
            data = {'login': False, 'msg': 'Usuario inválido'}
            return JsonResponse(data, safe=False)

        login(request, user)
        data = {'login': True, 'is_super': user.is_superuser, 'msg': ''}
        return JsonResponse(data, safe=False)
