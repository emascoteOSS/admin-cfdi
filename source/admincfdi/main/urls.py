from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^$', views.ViewUserLogin.as_view(), name='login'),
]
